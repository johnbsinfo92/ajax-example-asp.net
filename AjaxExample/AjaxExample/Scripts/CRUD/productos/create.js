﻿$(document).ready(function () {
    $('#save').click(function () {
        //crea un var para guardar los datos que están en los inputs de la vista.
        var productoDTO = {
            Precio: $('#Precio').val(),
            Marca: $('#Marca').val(),
            Categoria: $('#Categoria').val(),
            Proveedor: $('#Proveedor').val()
        };
        //llama al objeto Resource creado en el resource.js,
        //llama al create y le pasamos el var donde guardamos los datos de la vista
        //al .done es para devolver los datos despues de la llamada ajax y así agregarlos a la tabla con el .append
        ProductoCliente.create(productoDTO).done(function (producto) {
            var rows = '';
            rows += '<tr>';
            rows += '<td>' + producto.Precio + '</td>';
            rows += '<td>' + producto.Marca + '</td>';
            rows += '<td>' + producto.Categoria + '</td>';
            rows += '<td>' + producto.Proveedor + '</td>';
            rows += '</tr>';
            $('#productos').append(rows);
        }).fail(function (error) {
            console.log(error);
            alert("Ha ocurrido un error");
        });
    });
});
